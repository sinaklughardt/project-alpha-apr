from django.contrib import admin
from mycalendar.models import Event

# Register your models here.
admin.site.register(Event)
