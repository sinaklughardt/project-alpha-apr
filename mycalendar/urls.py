from django.urls import path
from mycalendar.views import index, CalendarView, event

urlpatterns = [
    path("index", index, name="calendar-index"),
    path("show/", CalendarView.as_view(), name="calendar"),
    path("event/new/", event, name="event_new"),
    path("event/edit/<int:id>", event, name="event_edit"),
]
