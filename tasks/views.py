from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from tasks.models import Task, Note
from tasks.forms import TaskForm, NoteForm
from projects.models import Project
import pandas as pd
import plotly.express as px
from plotly.offline import plot


# Create your views here.
@login_required
def task_list(request, id):
    task_list = Task.objects.filter(project=id)
    project = Project.objects.get(id=id)
    # task_list = get_object_or_404(Task, id=id)
    context = {"task_list": task_list, "project": project}
    return render(request, "tasks/task_list.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {"task_list": task_list}
    return render(request, "tasks/my_tasks.html", context)


@login_required
def show_notes(request, id):
    note_list = Note.objects.filter(task_id=id)
    task = Task.objects.get(id=id)
    if request.method == "POST":
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = NoteForm

    context = {"note_list": note_list, "task": task, "form": form}
    return render(request, "tasks/notes.html", context)


@login_required
def edit_note(request, id):
    post = get_object_or_404(Note, id=id)
    if request.method == "POST":
        form = NoteForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("show_notes", id=post.task.id)
    else:
        form = NoteForm(instance=post)
    context = {
        "post": post,
        "form": form,
    }
    return render(request, "tasks/edit_note.html", context)


@login_required
def delete_note(request, id):
    note = Note.objects.get(id=id)
    if request.method == "POST":
        note.delete()
        return redirect("show_notes", id=note.task.id)
    return render(request, "tasks/delete_note.html")


@login_required
def index(request):
    qs = Task.objects.all()
    projects_data = [
        {
            "Project": x.name,
            "Start": x.start_date,
            "Finish": x.due_date,
            "Assignee": x.assignee.username,
        }
        for x in qs
    ]
    df = pd.DataFrame(projects_data)
    fig = px.timeline(
        df, x_start="Start", x_end="Finish", y="Project", color="Assignee"
    )
    fig.update_yaxes(autorange="reversed")
    gantt_plot = plot(fig, output_type="div")
    context = {"plot_div": gantt_plot}
    return render(request, "tasks/index.html", context)
