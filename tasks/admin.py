from django.contrib import admin
from tasks.models import Task, Note


# Register your models here.
@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ("name", "id", "due_date")
@admin.register(Note)
class NoteAdmin(admin.ModelAdmin):
    list_display = ("task", "note", "task_id")
