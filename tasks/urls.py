from django.urls import path
from tasks.views import (
    task_list,
    create_task,
    show_my_tasks,
    show_notes,
    edit_note,
    delete_note,
    index,
)

urlpatterns = [
    path("<int:id>/", task_list, name="show_project"),
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:id>/notes/", show_notes, name="show_notes"),
    path("edit/<int:id>", edit_note, name="edit_note"),
    path("delete/<int:id>", delete_note, name="delete_note"),
    path("index/", index, name="index"),
]
