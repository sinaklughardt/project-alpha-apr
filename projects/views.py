from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project, Company
from projects.forms import ProjectForm, CompanyForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": list_projects}
    return render(request, "projects/list.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)

@login_required
def show_company(request, id):
    Company.objects.get(id=id)
    company = get_object_or_404(Company, id=id)
    context = {"company": company}
    return render(request, "company/show_company.html", context)

@login_required
def company_list(request):
    company_list = Company.objects.all()
    context = {"company_list": company_list}
    return render(request, "company/list.html", context)

@login_required
def create_company(request):
    if request.method == "POST":
        form = CompanyForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CompanyForm()
    context = {
        "form": form
    }
    return render(request, "company/create_company.html", context)
