from django.urls import path
from projects.views import list_projects, create_project, show_company, company_list, create_company

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("create/", create_project, name="create_project"),
    path("company/<int:id>",show_company, name="show_company"),
    path("companies/", company_list, name="company_list"),
    path("company/create/", create_company, name="create_company")
]
