# Generated by Django 4.1.7 on 2023-03-08 02:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0005_project_company"),
    ]

    operations = [
        migrations.AlterField(
            model_name="project",
            name="company",
            field=models.ForeignKey(
                default=1,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="projects",
                to="projects.company",
            ),
        ),
    ]
