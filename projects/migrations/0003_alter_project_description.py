# Generated by Django 4.1.7 on 2023-03-06 22:35

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0002_project_description_alter_project_owner"),
    ]

    operations = [
        migrations.AlterField(
            model_name="project",
            name="description",
            field=models.TextField(default="Please enter description"),
            preserve_default=False,
        ),
    ]
