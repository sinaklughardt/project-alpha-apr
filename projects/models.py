from django.db import models
from django.contrib.auth.models import User


class Company(models.Model):
    name = models.CharField(max_length=150)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    zip = models.CharField(max_length=5)
    email = models.EmailField()
    phone = models.CharField(max_length=15)

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(null=False)
    owner = models.ForeignKey(
        User, related_name="projects", on_delete=models.CASCADE, null=True
    )
    company = models.ForeignKey(
        Company, related_name="projects", on_delete=models.CASCADE, default=1
    )

    def __str__(self):
        return self.name
