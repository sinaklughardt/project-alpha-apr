from django.shortcuts import render, redirect, get_object_or_404
from accounts.forms import LogInForm, SignUpForm, ProfileForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from accounts.models import Profile


def user_login(request):
    if request.method == "POST":
        form = LogInForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("list_projects")

    else:
        form = LogInForm()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                create_profile(request, user)
                login(request, user)

                return redirect("list_projects")

            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = SignUpForm()
    context = {
        "form": form,
    }

    return render(request, "registration/signup.html", context)


def create_profile(request, user):
    form = ProfileForm(request.POST)
    if form.is_valid():
        profile = form.save(False)
        profile.user = user
        profile.pronouns = "they"
        profile.save()


@login_required
def show_user(request):
    user = request.user
    profile = user.user_profile
    context = {"user": user, "profile": profile}
    return render(request, "accounts/user_details.html", context)


@login_required
def edit_user(request):
    profile = get_object_or_404(Profile, user=request.user)
    user = request.user
    if request.method == "POST":
        form = ProfileForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            if len(request.POST["username"]) > 0:
                user.username = request.POST["username"]
            if len(request.POST["email"]) > 0:
                user.email = request.POST["email"]
            if request.POST["first-name"] == None:
                user.first_name = request.POST["first_name"]
            user.last_name = request.POST["last_name"]
            user.save()
            return redirect("show_user")
    else:
        form = ProfileForm(instance=profile)
    context = {"post": profile, "form": form}
    return render(request, "accounts/edit_user.html", context)
