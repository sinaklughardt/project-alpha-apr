from django.urls import path
from accounts.views import (
    user_login,
    user_logout,
    user_signup,
    show_user,
    edit_user,
)

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", user_signup, name="signup"),
    path("user/profile/", show_user, name="show_user"),
    path("user/profile/edit/", edit_user, name="edit_user"),
]
