# Create your models here.
from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    pronouns = models.CharField(max_length=150, null=True, blank=True)
    user = models.OneToOneField(
        User,
        related_name="user_profile",
        on_delete=models.CASCADE,
    )
    phone = models.CharField(max_length=15, null=True, blank=True)
